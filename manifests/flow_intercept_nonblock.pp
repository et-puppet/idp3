# Set up a custom "intercept flow".
#
#   $flow_base_dir = '/opt/shibboleth-idp/flows/intercept',

define idp3::flow_intercept_nonblock (
  $idp_dir,
  $flow_base_dir,
  $intercept_views_dir,
) {
  $flow_name_lc = downcase($name)
  $flow_dir     = "${flow_base_dir}/${name}"

  $beans_file      = "${flow_dir}/${flow_name_lc}-beans.xml"
  $flow_file       = "${flow_dir}/${flow_name_lc}-flow.xml"
  $view_file       = "${intercept_views_dir}/${flow_name_lc}.vm"
  $conf_file       = "${idp_dir}/conf/intercept/${flow_name_lc}-intercept-config.xml"

  # Create the flow directory
  file { $flow_dir:
    ensure  => directory
  }

  file { $beans_file:
    source  => "puppet:///modules/${module_name}/${beans_file}",
    require => File[$flow_dir],
  }

  file { $flow_file:
    source  => "puppet:///modules/${module_name}/${flow_file}",
    require => File[$flow_dir],
  }

  # Install the Velocity template
  file { $view_file:
    source  => "puppet:///modules/${module_name}/${view_file}",
    require => File[$flow_dir],
  }

  # The config file, e.g.,
  # conf/intercept/som-datasecurity-intercept-config.xml
  file { $conf_file:
    ensure  => present,
    source  => "puppet:///modules/${module_name}/${conf_file}",
    require => File[$flow_dir]
  }


}
