# Install the idp-custom-endpoint JAR file

class idp3::comparator_jar(
  $idp_dir,
  $idp_endpoint_comparator_url,
  $idp_endpoint_comparator_jar,
)
{
  $dest_dir = "${idp_dir}/edit-webapp/WEB-INF/lib"
  exec { 'add idp-custom-endpoint.jar':
    command => "/usr/bin/curl -s ${idp_endpoint_comparator_url} -o ${dest_dir}/${idp_endpoint_comparator_jar}",
    cwd     => $dest_dir,
    creates => "${dest_dir}/${idp_endpoint_comparator_jar}",
  }
}
