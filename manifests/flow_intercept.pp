# Set up a custom "intercept flow".
#
# $type: must be one 'block', 'inform', or 'redirect'.
#   Default: undef
#
# $flow_base_dir: the base directory where to put the '-flow.xml' and '-beans.xml' files.
#   Default: undef
#
# $intercept_views_dir: where to puth the velocity template file (for 'block' and 'inform'
#   flows intercepts only).
#   Default: undef
#
# Example.
# If the name of this define is 'my_intercept', then the the following
# files and directories are created/populated:
#
#  $flow_base_dir/my_intercept/  (directory)
#  $flow_base_dir/my_intercept/my_intercept-beans.xml
#  $flow_base_dir/my_intercept/my_intercept-flow.xml
#  $intercept_views_dir/my_intercept-flow.vm                 (blocking and informing only)
#  $idp_dir/conf/intercept/my_intercept-intercept-config.xml (informing only)

define idp3::flow_intercept (
  $type                = undef,
  $idp_dir             = undef,
  $flow_base_dir       = undef,
  $intercept_views_dir = undef,
) {

  if (!$type) {
    crit('missing required parameter type')
  }

  if (!($type =~ /block|inform|redirect/)) {
    crit("unknown type ${type}")
  }

  # Normalize the flow name.
  $flow_name_lc = downcase($name)

  # Step 1. Create the "-flow.xml" and "-beans.xml" files.
  $flow_dir   = "${flow_base_dir}/${flow_name_lc}"
  $beans_file = "${flow_dir}/${flow_name_lc}-beans.xml"
  $flow_file  = "${flow_dir}/${flow_name_lc}-flow.xml"

  # Create the flow directory
  file { $flow_dir:
    ensure  => directory
  }

  file { $beans_file:
    source  => "puppet:///modules/${module_name}/${beans_file}",
    require => File[$flow_dir],
  }

  file { $flow_file:
    source  => "puppet:///modules/${module_name}/${flow_file}",
    require => File[$flow_dir],
  }

  # Step 2. For blocking and informing types, add the velocity
  # view file.
  if ($type =~ /block|inform/) {
    if (!$intercept_views_dir) {
      crit('missing required intercept_views_dir parameter')
    }

    $view_file = "${intercept_views_dir}/${flow_name_lc}.vm"

    file { $view_file:
      ensure  => link,
      target  => "${idp_dir}/conf.local/views/intercept/${flow_name_lc}.vm",
      require => File[$flow_dir],
    }
  }

  # Step 3. For informing intercepts, add the config bean file.
  if ($type =~ /inform/) {
    $conf_file = "${idp_dir}/conf/intercept/${flow_name_lc}-intercept-config.xml"

    file { $conf_file:
      ensure  => present,
      source  => "puppet:///modules/${module_name}/${conf_file}",
      require => File[$flow_dir]
    }
  }

}
