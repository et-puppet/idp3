# Set up a custom "intercept flow" that BLOCKS further access.
#
#   $flow_base_dir = '/opt/shibboleth-idp/flows/intercept',

define idp3::flow_intercept_block (
  $flow_base_dir,
  $intercept_views_dir
) {
  $flow_name_lc = downcase($name)
  $flow_dir     = "${flow_base_dir}/${name}"

  $beans_file     = "${flow_dir}/${flow_name_lc}-beans.xml"
  $flow_file      = "${flow_dir}/${flow_name_lc}-flow.xml"
  $velocity_file  = "${intercept_views_dir}/${flow_name_lc}.vm"

  # Create the flow directory
  file { $flow_dir:
    ensure  => directory
  }

  file { $beans_file:
    source  => "puppet:///modules/${module_name}/${beans_file}",
    require => File[$flow_dir],
  }

  file { $flow_file:
    source  => "puppet:///modules/${module_name}/${flow_file}",
    require => File[$flow_dir],
  }

  file { $velocity_file:
    source  => "puppet:///modules/${module_name}/${velocity_file}",
    require => File[$flow_dir],
  }

}
