# configure the IdP
#
class idp3::idp (
  $idp_dir,
  $tomcat_user,
  $tomcat_group,
  $link_etc         = true,
  $extra_intercepts = [],
) {

  #
  # IdP Configuration
  #

  ### FILES ###
#  $tomcat_group=$idp3::tomcat_group
#  $tomcat_user=$idp3::tomcat_user
#  $idp_dir=$idp3::idp_dir
  #
  $idp_views_dir              = "${idp_dir}/views"
  $idp_views_intercept_dir    = "${idp_dir}/views/intercept"
  $idp_webapp_dir             = "${idp_dir}/webapp"
  $idp_webapp_images_dir      = "${idp_dir}/webapp/images"
  $idp_webapp_web_inf_dir     = "${idp_dir}/webapp/WEB-INF"
  $idp_webapp_css_dir         = "${idp_dir}/webapp/css"
  $idp_webapp_js_dir          = "${idp_dir}/webapp/js"
  $idp_messages_dir           = "${idp_dir}/messages"
  $idp_conf_authn_dir         = "${idp_dir}/conf/authn"
  $idp_conf_c14n_dir          = "${idp_dir}/conf/c14n"
  $idp_flows_authn_conditions = "${idp_dir}/flows/authn/conditions"
  $idp_flows_authn_duodefult  = "${idp_dir}/flows/authn/DuoDefault"
  $idp_flows_authn_duoforced  = "${idp_dir}/flows/authn/DuoForced"
  $idp_flows_authn_duosession = "${idp_dir}/flows/authn/DuoSession"
  $idp_system_flow_saml_dir   = "${idp_dir}/system/flows/saml"

  file { "${idp_dir}/metadata/google-metadata.xml":
    ensure  => link,
    owner   => $tomcat_user,
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/metadata/google-metadata.xml",
    require => File["${idp_dir}/metadata"],
  }

#  file { "${idp_dir}/webapp/images/stanford-logo.png":
#    ensure => link,
#    owner  => 'root',
#    group  => $tomcat_group,
#    mode   => '0644',
#    source => "puppet:///modules/${module_name}/idp/webapp/images/stanford-logo.png",
#  }

# MOVING TO shib-idp-config
#  ### /opt/shibboleth-idp/views
#  #  $idp_views_dir="${idp_dir}/views"
#  file { "${idp_views_dir}/duo.vm":
#    ensure  => file,
#    owner   => 'root',
#    group   => 'root',
#    mode    => '0644',
#    source  => "puppet:///modules/${module_name}${idp_views_dir}/duo.vm",
#    require => File["${idp_views_dir}"],
#  }

  file { "${idp_views_dir}/login.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/login.vm",
  }

  file { "${idp_views_dir}/error.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/error.vm",
  }

  file { "${idp_views_dir}/login-error.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/login-error.vm",
  }

  file { "${idp_views_dir}/login-warning.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/login-warning.vm",
  }

  file { "${idp_views_dir}/bottom.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/bottom.vm",
  }

  file { "${idp_views_dir}/duo.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/duo.vm",
  }

  file { "${idp_views_dir}/user-prefs.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/user-prefs.vm",
  }

  file { "${idp_views_dir}/logout.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/logout.vm",
  }

  file { "${idp_views_dir}/logout-complete.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/logout-complete.vm",
    require => File[$idp_views_dir],
  }

  file { "${idp_views_dir}/logout-propagate.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/logout-propagate.vm",
    require => File[$idp_views_dir],
  }

  file { "${idp_views_dir}/top.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/top.vm",
    require => File[$idp_views_dir],
  }

  file { "${idp_views_dir}/head.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/head.vm",
    require => File[$idp_views_dir],
  }


  file { "${idp_views_dir}/resolvertest.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/resolvertest.vm",
    require => File[$idp_views_dir],
  }

  file { "${idp_views_dir}/spnego-unavailable.vm":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/spnego-unavailable.vm",
    require => File[$idp_views_dir],
  }

  file { "${idp_views_dir}/user-prefs.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_views_dir}/user-prefs.js",
    require => File[$idp_views_dir],
  }

  ### /opt/shibboleth-idp/webapp/css
  #$idp_webapp_css_dir
  file { "${idp_webapp_css_dir}/bootstrap.min.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/bootstrap.min.css",
    require => File[$idp_webapp_css_dir],
  }

  file { "${idp_webapp_css_dir}/ie7.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/ie7.css",
    require => File[$idp_webapp_css_dir],
  }

  file { "${idp_webapp_css_dir}/ie8.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/ie8.css",
    require => File[$idp_webapp_css_dir],
  }

  file { "${idp_webapp_css_dir}/login.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/login.css",
    require => File[$idp_webapp_css_dir],
  }

  file { "${idp_webapp_css_dir}/su-identity.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/su-identity.css",
    require => File[$idp_webapp_css_dir],
  }

  ### /opt/shibboleth-idp/webapp/js
  #$idp_webapp_js_dir
  file { "${idp_webapp_js_dir}/bootstrap.min.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/bootstrap.min.js",
    require => File[$idp_webapp_js_dir],
  }

  file { "${idp_webapp_js_dir}/html5.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/html5.js",
    require => File[$idp_webapp_js_dir],
  }

  file { "${idp_webapp_js_dir}/jquery-1.11.1.min.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/jquery-1.11.1.min.js",
    require => File[$idp_webapp_js_dir],
  }

  file { "${idp_webapp_js_dir}/login.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/login.js",
    require => File[$idp_webapp_js_dir],
  }

  ### /opt/shibboleth-idp/webapp/images
  #$idp_webapp_images_dir

  file { "${idp_webapp_images_dir}/default.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/default.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/dummylogo-mobile.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/dummylogo-mobile.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/dummylogo.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/dummylogo.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/failure-32x32.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/failure-32x32.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/help14.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/help14.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/rt-arrow.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/rt-arrow.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/stanford-logo.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/stanford-logo.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/success-32x32.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/success-32x32.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/weblogin-header.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/weblogin-header.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/weblogin-header.svg":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/weblogin-header.svg",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_call.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_call.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_call_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_call_dk.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_duo.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_duo.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_duo_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_duo_dk.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_list.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_list.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_push.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_push.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_push_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_push_dk.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_sms.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_sms.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_sms_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_sms_dk.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_token.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_token.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/icon_token_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_token_dk.png",
    require => File[$idp_webapp_images_dir],
  }

  file { "${idp_webapp_images_dir}/loader.gif":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/loader.gif",
    require => File[$idp_webapp_images_dir],
  }

  #---special character needs sigle quote
  file { '/opt/shibboleth-idp/webapp/images/footer-stanford-logo@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/footer-stanford-logo@2x.png',
    require => File[$idp_webapp_images_dir],
  }

  file { '/opt/shibboleth-idp/webapp/images/brandbar-stanford-logo@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/brandbar-stanford-logo@2x.png',
    require => File[$idp_webapp_images_dir],
  }

  file { '/opt/shibboleth-idp/webapp/images/login-header@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/login-header@2x.png',
    require => File[$idp_webapp_images_dir],
  }

  #$idp_webapp_web_inf_dir="${idp_dir}/webapp/WEB-INF"
  file { "${idp_webapp_web_inf_dir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_web_inf_dir}/web.xml",
    require => File[$idp_webapp_web_inf_dir],
  }

  #$idp_webapp_dir="${idp_dir}/webapp"
  file { "${idp_webapp_dir}/error.jsp":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_dir}/error.jsp",
    require => File[$idp_webapp_dir],
  }

  file { "${idp_webapp_dir}/error-404.jsp":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_dir}/error-404.jsp",
    require => File[$idp_webapp_dir],
  }

  file { "${idp_webapp_dir}/favicon.ico":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_dir}/favicon.ico",
    require => File[$idp_webapp_dir],
  }

  file { "${idp_webapp_dir}/logo.png":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_dir}/logo.png",
    require => File[$idp_webapp_dir],
  }

  ### /opt/shibboleth-idp/conf/authn
  #$idp_conf_authn_dir="${idp_dir}/conf/authn"
  file { "${idp_conf_authn_dir}/general-authn.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/authn/general-authn.xml",
  }

  file { "${idp_conf_authn_dir}/authn-comparison.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/authn/authn-comparison.xml",
  }

  file { "${idp_conf_authn_dir}/duo-authn-config.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/authn/duo-authn-config.xml",
  }

  file { "${idp_conf_authn_dir}/mfa-authn-config.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/authn/mfa-authn-config.xml",
  }

#  file { "${idp_conf_authn_dir}/firstFactor.js":
#    ensure => link,
#    owner  => 'root',
#    group  => $tomcat_group,
#    mode   => '0644',
#    target => "${idp_dir}/conf.local/authn/firstFactor.js",
#  }
#
#  file { "${idp_conf_authn_dir}/secondFactor.js":
#    ensure => link,
#    owner  => 'root',
#    group  => $tomcat_group,
#    mode   => '0644',
#    target => "${idp_dir}/conf.local/authn/secondFactor.js",
#  }

  file { "${idp_conf_authn_dir}/password-authn-config.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_conf_authn_dir}/password-authn-config.xml",
    require => File[$idp_conf_authn_dir],
  }

  file { "${idp_conf_authn_dir}/spnego-authn-config.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_conf_authn_dir}/spnego-authn-config.xml",
    require => File[$idp_conf_authn_dir],
  }

  file { "${idp_conf_authn_dir}/x509-authn-config.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_conf_authn_dir}/x509-authn-config.xml",
    require => File[$idp_conf_authn_dir],
  }

  ### /opt/shibboleth-idp/conf/c14n
  #$idp_conf_c14n_dir="${idp_dir}/conf/c14n"
  file { "${idp_conf_c14n_dir}/x500-subject-c14n-config.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/x500-subject-c14n-config.xml",
  }

  ### SYMLINKS ###

  ### /opt/shibboleth-idp/messages
  #$idp_messages_dir="${idp_dir}/messages"
  file { "${idp_messages_dir}/messages.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/messages/messages.properties",
  }

  file { "${idp_messages_dir}/authn-messages.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/messages/authn-messages.properties",
  }

  file { "${idp_messages_dir}/consent-messages.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/messages/consent-messages.properties",
  }

  file { "${idp_messages_dir}/error-messages.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/messages/error-messages.properties",
  }

  file { "${idp_messages_dir}/custom.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/messages/custom.properties",
  }

  file { "${idp_dir}/conf/saml-nameid.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/saml-nameid.xml",
  }

  file { "${idp_dir}/conf/access-control.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/access-control.xml",
  }

  file { "${idp_dir}/conf/attribute-filter.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/attribute-filter.xml",
  }

  file { "${idp_dir}/conf/attribute-resolver.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/attribute-resolver.xml",
  }

  file { "${idp_dir}/conf/attribute-resolver-amazon-auto.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/attribute-resolver-amazon-auto.xml",
  }

  file { "${idp_dir}/conf/attribute-resolver-amazon-exception.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/attribute-resolver-amazon-exception.xml",
  }

  file { "${idp_dir}/conf/activation-conditions-relyingparty-profile.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/activation-conditions-relyingparty-profile.xml",
  }

  file { "${idp_dir}/conf/scripted.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/scripted.xml",
  }

  file { "${idp_dir}/conf/activation-conditions-saml-nameid-sourceid.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/activation-conditions-saml-nameid-sourceid.xml",
  }

  file { "${idp_dir}/conf/activation-conditions-attribute-resolver.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/activation-conditions-attribute-resolver.xml",
  }

  file { "${idp_dir}/conf/audit.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/audit.xml",
  }

  ## conf/intercept/profile-intercept.xml.
  file { "${idp_dir}/conf/intercept/profile-intercept.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/profile-intercept.xml",
  }

  # Install the krb5_jaas.config file. This file contains the Kerberos
  # principal name and keytab path. It expects that the two variables
  # $ldap_principal_name and $ldap_keytab_path are set.
  #
  # There should be no reason to change $ldap_keytab_path, but
  # $ldap_principal_name depends on the environment (dev, uat,
  # prod, etc.).
  file { "${idp_dir}/conf/authn/krb5_jaas.config":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/krb5_jaas.config",
  }

  file { "${idp_dir}/conf/idp.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/idp.properties",
  }

  file { "${idp_dir}/conf/ldap.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/ldap.properties",
  }

  file { "${idp_dir}/conf/logback.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/logback.xml",
  }

  file { "${idp_dir}/conf/metadata-providers.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/metadata-providers.xml",
  }

  file { "${idp_dir}/conf/relying-party.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/relying-party.xml",
  }

  file { "${idp_dir}/conf/saml-nameid.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/saml-nameid.properties",
  }

  file { "${idp_dir}/conf/services.properties":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/services.properties",
  }

  file { "${idp_dir}/conf/services.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/services.xml",
  }

  file { "${idp_dir}/conf/global.xml":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/global.xml",
  }

  file { "${idp_dir}/conf/errors.xml":
    ensure => file,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    source => "puppet:///modules/${module_name}${idp_dir}/conf/errors.xml",
  }

  file { "${idp_dir}/metadata/idp-metadata.xml":
    ensure  => link,
    owner   => $tomcat_user,
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/metadata/idp-metadata.xml",
    require => File["${idp_dir}/metadata"],
  }



  #
  # Link /etc/shibboleth-idp to /opt/shibboleth-idp.
  # In some cases we _don't_ want to stick the entire directory
  # /opt/shibboleth-idp inside /etc/shibboleth, so only link
  # it the $link_etc parameter us true.
  #
  if ($link_etc) {
    file { '/etc/shibboleth-idp':
      ensure  => link,
      owner   => 0,
      group   => 0,
      target  => $idp_dir,
      require => File[$idp_dir],
    }
  }

  # Custom intercept flows
  $flow_base_dir = "${idp_dir}/flows/intercept"

  ## conf/intercept/intercept-events-flow.xml
  file { "${idp_dir}/conf/intercept/intercept-events-flow.xml":
    ensure => file,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    source => "puppet:///modules/${module_name}${idp_dir}/conf/intercept/intercept-events-flow.xml",
  }

  ## The School of Medicine (informing)
  idp3::flow_intercept { 'som-datasecurity':
    type                => 'informing',
    idp_dir             => $idp_dir,
    flow_base_dir       => $flow_base_dir,
    intercept_views_dir => $idp_views_intercept_dir,
  }

  ## Security Video (blocking)
  idp3::flow_intercept { 'security-video-required':
    type                => 'blocking',
    idp_dir             => $idp_dir,
    flow_base_dir       => $flow_base_dir,
    intercept_views_dir => $idp_views_intercept_dir,
  }

  ## Security Video (informing)
  idp3::flow_intercept { 'security-video-warning':
    type                => 'informing',
    idp_dir             => $idp_dir,
    flow_base_dir       => $flow_base_dir,
    intercept_views_dir => $idp_views_intercept_dir,
  }

  ## Password Expired (redirecting)
  # flows/authn/conditions/expired-password/
  idp3::flow_intercept { 'expired-password':
    type          => 'redirecting',
    idp_dir       => $idp_dir,
    flow_base_dir => $idp_flows_authn_conditions,
  }

  ## Password Expired for non-username/password authentication (i.e., certs) (blocking)
  idp3::flow_intercept { 'non-unpw-expired-password':
    type                => 'blocking',
    idp_dir             => $idp_dir,
    flow_base_dir       => $flow_base_dir,
    intercept_views_dir => $idp_views_intercept_dir,
  }

  ## Some ad-hoc intercept views
  file { "${idp_views_dir}/security-video-buttons.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/security-video-buttons.vm",
  }

  file { "${idp_views_dir}/security-video-setup.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/security-video-setup.vm",
  }

  file { "${idp_views_dir}/intercept/terms-of-use.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/intercept/terms-of-use.vm",
  }

  file { "${idp_views_dir}/intercept/attribute-release.vm":
    ensure => link,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/intercept/attribute-release.vm",
  }

  ###
  ### password expiring intercept
  file { "${idp_dir}/conf/intercept/expiring-password-intercept-config.xml":
    ensure => file,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    source => "puppet:///modules/${module_name}${idp_dir}/conf/intercept/expiring-password-intercept-config.xml",
  }

  file { "${idp_views_intercept_dir}/expiring-password.vm":
    ensure => link,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    target => "${idp_dir}/conf.local/views/intercept/expiring-password.vm",
  }


  ###
  # bean file needed for redirects between login.stanford.edu and idp.stanford.edu
  # $idp_system_flow_saml_dir   = "${idp_dir}/system/flows/saml"
  file { "${idp_system_flow_saml_dir}/security-beans.xml":
    ensure => file,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0644',
    source => "puppet:///modules/${module_name}${idp_system_flow_saml_dir}/security-beans.xml",
  }

  ###
  # install 3 Duo Flows directories with beans and flows
  # $idp_flows_authn_duodefult = "${idp_dir}/flows/authn/DuoDefault"
  # $idp_flows_authn_duoforced = "${idp_dir}/flows/authn/DuoForced"
  # $idp_flows_authn_duosession = "${idp_dir}/flows/authn/DuoSession"
  file { "${idp_flows_authn_duodefult}/duodefault-authn-beans.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duodefult}/duodefault-authn-beans.xml",
    require => File[$idp_flows_authn_duodefult],
  }

  file { "${idp_flows_authn_duodefult}/duodefault-authn-flow.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duodefult}/duodefault-authn-flow.xml",
    require => File[$idp_flows_authn_duodefult],
  }

  file { "${idp_flows_authn_duoforced}/duoforced-authn-beans.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duoforced}/duoforced-authn-beans.xml",
    require => File[$idp_flows_authn_duoforced],
  }

  file { "${idp_flows_authn_duoforced}/duoforced-authn-flow.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duoforced}/duoforced-authn-flow.xml",
    require => File[$idp_flows_authn_duoforced],
  }

  file { "${idp_flows_authn_duosession}/duosession-authn-beans.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duosession}/duosession-authn-beans.xml",
    require => File[$idp_flows_authn_duosession],
  }

  file { "${idp_flows_authn_duosession}/duosession-authn-flow.xml":
    ensure  => file,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_flows_authn_duosession}/duosession-authn-flow.xml",
    require => File[$idp_flows_authn_duosession],
  }


}
