# Create several needed directories in the Shibboleth IdP home directory.
#
# Pass in home directory via the parameter $idp_dir.

# $create_conf_local_dirs: If set to true (the default), create the
#   conf.local directories.  This is OK if you run idp_config separately
#   from this manifest, but in the case where you run them together (for
#   example, in the traditional server context), the idp_config creates
#   them.
# Default: true

class idp3::directories (
  $idp_dir,
  $tomcat_user,
  $tomcat_group,
  $copy_conf_local_credentials = false,
  $create_conf_local_dirs      = true,
) {

  if (!$idp_dir) {
    fail("missing required parameter idp_dir (Shibboleth IdP home directory)")
  }

  if (!$tomcat_group) {
    fail("missing required parameter tomcat_group")
  }

  file {
    [
      "${idp_dir}/edit-webapp",
      "${idp_dir}/edit-webapp/WEB-INF",
      "${idp_dir}/edit-webapp/WEB-INF/lib",
      "${idp_dir}/flows/authn/conditions",
      "${idp_dir}/flows/authn/DuoDefault",
      "${idp_dir}/flows/authn/DuoForced",
      "${idp_dir}/flows/authn/DuoSession",
      "${idp_dir}/flows/intercept",
      "${idp_dir}/messages",
      "${idp_dir}/views",
      "${idp_dir}/views/intercept",
      "${idp_dir}/webapp",
      "${idp_dir}/webapp/images",
      "${idp_dir}/webapp/css",
      "${idp_dir}/webapp/js",
      "${idp_dir}/webapp/WEB-INF",
      "${idp_dir}/metadata",
      "${idp_dir}/conf/authn",
      "${idp_dir}/system/flows/saml",
    ]:
    ensure => directory,
    owner  => 'root',
    group  => $tomcat_group,
    mode   => '0755',
  }

  if ($create_conf_local_dirs) {
    file {
      [
        "${idp_dir}/conf.local",
        "${idp_dir}/conf.local/credentials",
        "${idp_dir}/conf.local/messages",
        "${idp_dir}/conf.local/authn",
      ]:
      ensure => directory,
      owner  => 'root',
      group  => $tomcat_group,
      mode   => '0755',
    }
  }

  # For "traditional" servers we want to copy all the contents of
  # /opt/shibboleth-idp/conf.local/credentials into
  # /opt/shibboileth-idp/credentials.
  if ($copy_conf_local_credentials) {
    file { "${idp_dir}/credentials":
      ensure  => directory,
      source  => "${idp_dir}/conf.local/credentials",
      recurse => true,
    }
  } else {
    file { "${idp_dir}/credentials":
      ensure => directory,
      owner  => 'root',
      group  => $tomcat_group,
      mode   => '0755',
    }
  }


}
