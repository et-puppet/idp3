<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <script src="js/jquery-1.11.1.min.js"></script>
    <!-- bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" href="css/su-identity.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="js/html5.js"></script>
    <![endif]-->
        <!--[if IE 8]>
      <link rel="stylesheet" type="text/css" href="css/ie8.css" />
    <![endif]-->
        <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="css/ie7.css" />
    <![endif]-->
    <script src="js/login.js"></script>
    <!-- login CSS -->
    <link rel="stylesheet" type="text/css" href="css/login.css" />
    <title>Stanford Login</title>
</head>

<body>
    <div id="su-wrap">
        <!-- #su-wrap start -->
        <div id="su-content">
            <!-- #su-content start -->
            <!-- Brandbar snippet start -->
            <div id="brandbar">
                <div class="container">
                    <a href="http://www.stanford.edu"> <img src="images/brandbar-stanford-logo@2x.png" alt="Stanford University" width="153" height="22"> </a>
                </div>
                <!-- .container end -->
            </div>
            <!-- #brandbar end -->
            <!-- Brandbar snippet end -->
            <div id="header">
                <div class="container">
                    <div style="margin: 1em 10px 0 0;">
                        <img src="images/login-header@2x.png" alt="Stanford Login" width="222" height="37">
                        <h1 class="pagetitle">Stanford Login</h1>
                    </div>
                </div>
                <!-- .container end -->
            </div>
            <div id="content">
                <div class="container">
                    <div class="error-box">
                        <h2>Sorry ... something went wrong</h2>
                        <p>An error occurred while processing your request. Please close this window and try again.</p>
                        <p>If you need further assistance, please contact the <a href="https://stanford.service-now.com/it_services?id=sc_cat_item&sys_id=21cfc2684fdf6e0054c23f828110c77e">Help Desk</a>.</p>
                    </div>
                </div>
                <!-- .container end -->
            </div>
            <!-- .content end -->
        </div>
        <!-- #su-content end -->
    </div>
    <!-- #su-wrap end -->
    <!-- Global footer snippet start -->
    <div id="global-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-2" id="bottom-logo"> <a href="http://www.stanford.edu"> <img width="105" height="49" alt="Stanford University" src="images/footer-stanford-logo@2x.png"> </a> </div>
                <!-- #bottom-logo end -->
                <div class="col-xs-6 col-sm-10" id="bottom-text">
                    <ul class="list-inline">
                        <li class="home"><a href="https://www.stanford.edu">Stanford Home</a></li>
                        <li class="maps alt"><a data-ua-label="global-footer" data-ua-action="visit.stanford.edu/plan/maps" class="su-link" href="http://visit.stanford.edu/plan/">Maps &amp; Directions</a></li>
                        <li class="search-stanford"><a href="https://www.stanford.edu/search/">Search Stanford</a></li>
                        <li class="terms alt"><a href="https://www.stanford.edu/site/terms.html">Terms of Use</a></li>
                        <li class="emergency-info"><a data-ua-label="global-footer" class="su-link" href="http://emergency.stanford.edu/">Emergency Info</a></li>
                    </ul>
                </div>
                <p class="copyright vcard col-sm-10 col-sm-offset-2 col-xs-12">&copy; <span class="fn org">Stanford University</span>.&nbsp; <span class="adr"> <span class="locality">Stanford</span>, <span class="region">California</span> <span class="postal-code">94305</span></span>. <span id="termsofuse"><a data-ua-label="global-footer" data-ua-action="copyright-complaints" class="su-link" href="http://www.stanford.edu/group/security/dmca.html">Copyright Complaints</a>&nbsp;&nbsp;&nbsp;<a data-ua-label="global-footer" data-ua-action="trademark-notice" class="su-link" href="https://adminguide.stanford.edu/chapter-1/subchapter-5/policy-1-5-4">Trademark Notice</a></span></p>
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </div>
    <!-- global-footer end -->
</body>

</html>