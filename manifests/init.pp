# Class: idp3
# ===========================
#
# Class to configure tomcat and shibboleth-idp for a Stanford-ish IdP
#
# Examples
# --------
#
# @example
#
#    include 'idp3'
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
# Vivien Wu <vivienwu@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class idp3 (
  $idp_dir,
  $conf_dir,
  $conf_link,
  $tomcat_user,
  $tomcat_group,
  $tomcat_uid,
  $tomcat_gid,
  $tomcat_home,
  $tomcat_confdir,
  $tomcat_backport,
  $java_backport,
  $java_home,
  $java_ca_cert,
  $krb5_pkg,
  $gssapi_pkg,
  $tomcat_pkg,
  $java_pkg,
#  $jstl_pkg,
#  $jstl_jar,
  $ldap_pkg,
  $mysql_pkg,
  $mysql_jar,
  $authbind_pkg,
  $libaprutil1_pkg,
  $servlet_pkg,
  $libtcnative_backport,
  $libtcnative_pkg,
  $idp_url,
  $idp_hash,
  $idp_alg,
  $hikaricp_jar,
  $hikaricp_url,
  $hikaricp_hash,
  $hikaricp_alg,
  $commondbcp2_jar,
  $commondbcp2_url,
  $commondbcp2_hash,
  $commondbcp2_alg,
  $commonpool_jar,
  $commonpool_url,
  $commonpool_hash,
  $commonpool_alg,
  $idpsupport_jar,
  $idpsupport_url,
  $idpsupport_hash,
  $idpsupport_alg,
  $jstl_1_2_jar,
  $jstl_1_2_url,
  $jstl_1_2_hash,
  $jstl_1_2_alg,
  $cookie_war,
  $cookie_war_link,
  $cookie_dir,
  $cookie_url,
  $cookie_hash,
  $cookie_alg,
) {

  include archive
  include apt

  # Sometimes we need the backport version of tomcat
  if ($idp3::tomcat_backport) {
    apt::pin { 'tomcat':
      packages => [
        $tomcat_pkg,
        "${tomcat_pkg}-common",
        "lib${tomcat_pkg}-java",
        'libecj-java',
        $servlet_pkg,
      ],
      priority => 500,
      codename => "${::lsbdistcodename}-backports",
      notify   => Exec['apt_update'],
    }
  }

  #-- Java8 requires backport version for jessie
  if ($idp3::java_backport) {
    apt::pin { 'java8':
      packages => [
        $java_ca_cert,
        $java_pkg,
      ],
      priority => 500,
      codename => "${::lsbdistcodename}-backports",
      notify   => Exec['apt_update'],
    }
  }

  #-- libtcnative also needs backports version
  if ($idp3::libtcnative_backport) {
    apt::pin { 'libtcnative':
      packages => [
        'libssl1.0.0',
        $libtcnative_pkg,
      ],
      priority => 500,
      codename => "${::lsbdistcodename}-backports",
      notify   => Exec['apt_update'],
    }
  }


  Exec['apt_update'] -> Package<| |>

  Package[$java_pkg] -> Package[$tomcat_pkg]
#  Package[$java_pkg] -> Package[$jstl_pkg]
  Package[$java_pkg] -> Package[$mysql_pkg]

  package {
    [
      $krb5_pkg,
      $gssapi_pkg,
      $java_pkg,
      $tomcat_pkg,
#      $jstl_pkg,
      $ldap_pkg,
      $mysql_pkg,
      $authbind_pkg,
      $libaprutil1_pkg,
      $servlet_pkg,
      $libtcnative_pkg,
    ]:
    ensure => latest,
  }

  group { $tomcat_group:
    ensure => present,
    gid    => $tomcat_gid,
    system => true,
  }

  user { $tomcat_user:
    ensure  => present,
    uid     => $tomcat_uid,
    gid     => $tomcat_gid,
    system  => true,
    home    => $tomcat_home,
    shell   => '/bin/false',
    comment => 'tomcat',
    require => Group[$tomcat_group],
    before  => Package[$tomcat_pkg],
  }

  #
  # IdP Install
  #

  file { 'idp-dir':
    ensure  => directory,
    path    => $idp_dir,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0755',
    require => Package[$tomcat_pkg],
  }

  archive { 'idp':
    ensure          => present,
    extract         => true,
    extract_path    => $idp_dir,
    extract_command => 'tar xfz %s --strip-components=1',
    source          => $idp_url,
    checksum        => $idp_hash,
    checksum_type   => $idp_alg,
    path            => '/tmp/idp.tar.gz',
    creates         => "${idp_dir}/bin/install.sh",
    cleanup         => true,
    require         => File['idp-dir'],
  }


  # Create needed directories in the shibboleth-idp home directory.
  class { 'idp3::directories':
    idp_dir      => $idp_dir,
    tomcat_user  => $tomcat_user,
    tomcat_group => $tomcat_group,
    require      => Archive['idp'],
  }

  archive { 'hikaricp':
    ensure        => present,
    extract       => false,
    source        => $hikaricp_url,
    checksum      => $hikaricp_hash,
    checksum_type => $hikaricp_alg,
    path          => "${idp_dir}/edit-webapp/WEB-INF/lib/${hikaricp_jar}",
    require       => File["${idp_dir}/edit-webapp/WEB-INF/lib"],
  }

  archive { 'commondbcp2':
    ensure        => present,
    extract       => false,
    source        => $commondbcp2_url,
    checksum      => $commondbcp2_hash,
    checksum_type => $commondbcp2_alg,
    path          => "${idp_dir}/edit-webapp/WEB-INF/lib/${commondbcp2_jar}",
    require       => File["${idp_dir}/edit-webapp/WEB-INF/lib"],
  }

  archive { 'commonpool':
    ensure        => present,
    extract       => false,
    source        => $commonpool_url,
    checksum      => $commonpool_hash,
    checksum_type => $commonpool_alg,
    path          => "${idp_dir}/edit-webapp/WEB-INF/lib/${commonpool_jar}",
    require       => File["${idp_dir}/edit-webapp/WEB-INF/lib"],
  }

  archive { 'idpsupport':
    ensure        => present,
    extract       => false,
    source        => $idpsupport_url,
    checksum      => $idpsupport_hash,
    checksum_type => $idpsupport_alg,
    path          => "${idp_dir}/edit-webapp/WEB-INF/lib/${idpsupport_jar}",
    require       => File["${idp_dir}/edit-webapp/WEB-INF/lib"],
  }

  archive { 'jstl_1_2':
    ensure        => present,
    extract       => false,
    source        => $jstl_1_2_url,
    checksum      => $jstl_1_2_hash,
    checksum_type => $jstl_1_2_alg,
    path          => "${idp_dir}/edit-webapp/WEB-INF/lib/${jstl_1_2_jar}",
    require       => File["${idp_dir}/edit-webapp/WEB-INF/lib"],
  }


  exec { 'add mysql link':
    command => "/bin/ln -s ${mysql_jar} ${idp_dir}/edit-webapp/WEB-INF/lib",
    cwd     => $idp_dir,
    require => [
      File["${idp_dir}/edit-webapp/WEB-INF/lib"],
      Package[$mysql_pkg],
      Package[$java_pkg],
    ],
  }

  file { 'cookie-dir':
    ensure  => directory,
    path    => $cookie_dir,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0755',
    require => Package[$tomcat_pkg],
  }

  archive { 'cookiewar':
    ensure        => present,
    extract       => false,
    source        => $cookie_url,
    checksum      => $cookie_hash,
    checksum_type => $cookie_alg,
    path          => "${cookie_dir}/${cookie_war}",
    require       => File['cookie-dir'],
  }


  file { "${cookie_dir}/${cookie_war_link}":
    ensure  => link,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    target  => "${cookie_war}",
    require => Archive['cookiewar'],
  }

  exec { 'install IdP':
    command     => "${idp_dir}/bin/build.sh build-war -Didp.target.dir=${idp_dir}",
    cwd         => $idp_dir,
    environment => [ "JAVA_HOME=${java_home}" ],
    require     => [
      Exec['add mysql link'],
      Archive['jstl_1_2'],
      Archive['hikaricp'],
      Archive['commondbcp2'],
      Archive['commonpool'],
      Archive['idpsupport'],
    ],
    notify      => Exec['fix ownership'],
  }

  exec { 'fix ownership':
    command => "/bin/chown -R root:${tomcat_group} ${idp_dir}",
    notify  => Exec['fix permissions'],
  }

  # Use the Puppet 3 compatible version of multi-resource dependency so
  # that we can use this with older Puppet code.
  exec { 'fix permissions':
    command => "/bin/chmod -R u+rwX,g+rX ${idp_dir}",
    notify  => [
      File["${idp3::idp_dir}/metadata"],
    ],
  }

  # IdP Configuration
  class { 'idp3::idp':
    idp_dir      => $idp_dir,
    tomcat_user  => $tomcat_user,
    tomcat_group => $tomcat_group,
  }

  # Kerberos Configuration
  file { '/etc/krb5.conf':
    ensure  => link,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    target  => "${idp_dir}/conf.local/krb5.conf",
    require => Package[$krb5_pkg],
  }

  # Tomcat Configuration
  class { 'idp3::tomcat':
    idp_dir        => $idp_dir,
    tomcat_pkg     => $tomcat_pkg,
    tomcat_confdir => $tomcat_confdir,
    tomcat_user    => $tomcat_user,
    tomcat_group   => $tomcat_group,
    cookie_dir     => $cookie_dir,
    cookie_war     => $cookie_war,
  }

}
