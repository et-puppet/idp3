# configure tomcat
#
class idp3::tomcat(
  $idp_dir,
  $tomcat_pkg,
  $tomcat_confdir,
  $tomcat_user,
  $tomcat_group,
  $cookie_dir,
  $cookie_war,
) {
  #
  # Tomcat Configuration
  #

  ## /etc/default/tomcat8
  file { '/etc/default/tomcat8':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/etc/default/tomcat8",
    require => Package[$tomcat_pkg],
  }


  ## create /etc/authbind/byport/80 and 443 directory
  file {
    [
      '/etc/authbind/byport',
      '/etc/authbind/byport/80',
      '/etc/authbind/byport/443',
    ]:
    ensure  => directory,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0755',
    require => Package[$tomcat_pkg],
  }

  ## ensure the private key directory is readable by the tomcat user
  file { '/etc/ssl/private':
    ensure  => directory,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0750',
    require => Package[$tomcat_pkg],
  }

  file { '/etc/ssl/certs':
    ensure  => directory,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0750',
    require => Package[$tomcat_pkg],
  }

  ## symlink the ssl key/cert/chain
  file { '/etc/ssl/private/server.key':
    ensure  => link,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/credentials/server.key",
    require => File['/etc/ssl/private'],
  }

  file { '/etc/ssl/certs/server.pem':
    ensure  => link,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/credentials/server.pem",
    require => File['/etc/ssl/certs'],
  }

  file { '/etc/ssl/certs/server-chain.pem':
    ensure  => link,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/credentials/server-chain.pem",
    require => File['/etc/ssl/certs'],
  }

  ## primary tomcat8 configuration file
  file { "${tomcat_confdir}/server.xml":
    ensure  => link,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0644',
    target  => "${idp_dir}/conf.local/tomcat/server.xml",
    require => Package[$tomcat_pkg],
  }

  file { "${tomcat_confdir}/context.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${tomcat_confdir}/context.xml",
    require => Package[$tomcat_pkg],
  }

  file { "${tomcat_confdir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${tomcat_confdir}/web.xml",
    require => Package[$tomcat_pkg],
  }

  #--- create /var/lib/tomcat8/webapps sub-directories
  $webapps_root_dir="/var/lib/${tomcat_user}/webapps/ROOT"
  $webapps_web_inf_dir="/var/lib/${tomcat_user}/webapps/ROOT/WEB-INF"

  $webapps_css_dir="/var/lib/${tomcat_user}/webapps/ROOT/css"
  $idp_webapp_css_dir="${idp_dir}/webapp/css"
  $idp_webapp_dir="${idp_dir}/webapp"

  $webapps_images_dir="/var/lib/${tomcat_user}/webapps/ROOT/images"
  $idp_webapp_images_dir="${idp_dir}/webapp/images"

  $webapps_js_dir="/var/lib/${tomcat_user}/webapps/ROOT/js"
  $idp_webapp_js_dir="${idp_dir}/webapp/js"

  $webapps_meta_inf_dir="/var/lib/${tomcat_user}/webapps/ROOT/META-INF"

  $idp_redirect_dir='/opt/idp-redirect'
  $idp_redirect_meta_inf_dir='/opt/idp-redirect/META-INF'
  $idp_redirect_web_inf_dir='/opt/idp-redirect/WEB-INF'
  file {
    [
      $webapps_root_dir,
      $webapps_web_inf_dir,
      $webapps_css_dir,
      $webapps_images_dir,
      $webapps_js_dir,
      $webapps_meta_inf_dir,
      $idp_redirect_dir,
      $idp_redirect_meta_inf_dir,
      $idp_redirect_web_inf_dir,
    ]:
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package[$tomcat_pkg],
  }


  #-- /var/lib/tomcat8/webapps/ROOT
  #$webapps_root_dir
  file { "${webapps_root_dir}/idp.crt":
    ensure  => link,
    owner   => 'root',
    group   => 'root',
    target  => "${idp_dir}/conf.local/credentials/idp-saml.pem",
    require => File[$webapps_web_inf_dir],
  }

  #-- keep index.html from previous version
  file { "${webapps_root_dir}/index.html":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${webapps_root_dir}/index.html",
    require => File[$webapps_root_dir],
  }

  file { "${webapps_root_dir}/favicon.ico":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${webapps_root_dir}/favicon.ico",
    require => File[$webapps_root_dir],
  }

  file { "${webapps_root_dir}/logo.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_dir}/logo.png",
    require => File[$webapps_root_dir],
  }


  ### /var/lib/tomcat8/webapps/ROOT/WEB-INF
  file { "${webapps_web_inf_dir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${webapps_web_inf_dir}/web.xml",
    require => File[$webapps_web_inf_dir],
  }

  file { "${webapps_web_inf_dir}/rewrite.config":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${webapps_web_inf_dir}/rewrite.config",
    require => File[$webapps_web_inf_dir],
  }

  ### /var/lib/tomcat8/webapps/ROOT/css
  file { "${webapps_css_dir}/bootstrap.min.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/bootstrap.min.css",
    require => File[$webapps_css_dir],
  }

  file { "${webapps_css_dir}/ie7.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/ie7.css",
    require => File[$webapps_css_dir],
  }
  file { "${webapps_css_dir}/ie8.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/ie8.css",
    require => File[$webapps_css_dir],
  }
  file { "${webapps_css_dir}/login.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/login.css",
    require => File[$webapps_css_dir],
  }
  file { "${webapps_css_dir}/su-identity.css":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_css_dir}/su-identity.css",
    require => File[$webapps_css_dir],
  }

  ### /var/lib/tomcat8/webapps/ROOT/images
  #    "$webapps_images_dir"
  file { '/var/lib/tomcat8/webapps/ROOT/images/brandbar-stanford-logo@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/brandbar-stanford-logo@2x.png',
    require => File[$webapps_images_dir],
  }

  file { '/var/lib/tomcat8/webapps/ROOT/images/footer-stanford-logo@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/footer-stanford-logo@2x.png',
    require => File[$webapps_images_dir],
  }

  file { '/var/lib/tomcat8/webapps/ROOT/images/login-header@2x.png':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/idp3/opt/shibboleth-idp/webapp/images/login-header@2x.png',
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/default.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/default.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/help14.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/help14.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/icon_call.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_call.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/icon_call_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_call_dk.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/icon_duo.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_duo.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/icon_duo_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_duo_dk.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_list.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_list.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_push.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_push.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_push_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_push_dk.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_sms.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_sms.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_sms_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_sms_dk.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_token.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_token.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/icon_token_dk.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/icon_token_dk.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/loader.gif":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/loader.gif",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/rt-arrow.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/rt-arrow.png",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/signature.gif":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/signature.gif",
    require => File[$webapps_images_dir],
  }

  file { "${webapps_images_dir}/weblogin-header.png":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/weblogin-header.png",
    require => File[$webapps_images_dir],
  }
  file { "${webapps_images_dir}/weblogin-header.svg":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_images_dir}/weblogin-header.svg",
    require => File[$webapps_images_dir],
  }

  ### /var/lib/tomcat8/webapps/ROOT/js
  #    "$webapps_js_dir",
  file { "${webapps_js_dir}/bootstrap.min.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/bootstrap.min.js",
    require => File[$webapps_js_dir],
  }

  file { "${webapps_js_dir}/html5.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/html5.js",
    require => File[$webapps_js_dir],
  }

  file { "${webapps_js_dir}/jquery-1.11.1.min.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/jquery-1.11.1.min.js",
    require => File[$webapps_js_dir],
  }

  file { "${webapps_js_dir}/login.js":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_webapp_js_dir}/login.js",
    require => File[$webapps_js_dir],
  }

  ### /var/lib/tomcat8/webapps/ROOT/META-INF
  #    "$webapps_meta_inf_dir",
  file { "${webapps_meta_inf_dir}/context.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${webapps_meta_inf_dir}/context.xml",
    require => File[$webapps_meta_inf_dir],
  }

  ### /opt/idp-redirect/
  #	$idp_redirect_meta_inf_dir="/opt/idp-redirect/META-INF"
  file { "${idp_redirect_meta_inf_dir}/context.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_redirect_meta_inf_dir}/context.xml",
    require => File[$idp_redirect_meta_inf_dir],
  }

  ### /opt/idp-redirect/
  #	$idp_redirect_web_inf_dir="/opt/idp-redirect/WEB-INF"
  file { "${idp_redirect_web_inf_dir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_redirect_web_inf_dir}/web.xml",
    require => File[$idp_redirect_web_inf_dir],
  }

  file { "${idp_redirect_web_inf_dir}/rewrite.config":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}${idp_redirect_web_inf_dir}/rewrite.config",
    require => File[$idp_redirect_web_inf_dir],
  }



}
