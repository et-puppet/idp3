"use strict";

function createCookie(cookieName, value, seconds) {
  var date    = new Date(Date.now() + (seconds * 1000)),
      expires = date.toGMTString(),
      path    = '$environment.getProperty("idp.cookie.path", $request.getContextPath())',
      cookie  = cookieName + '=' + value
              + ';expires=' + expires
              + (path.length > 0 ? '; path=' + path : '');

  document.cookie = cookie
  console.log('Created cookie: ' + cookie);
}

function eraseCookie(name) {
  console.log('Erasing cookie ' + name);
  createCookie(name, '', -31536000);
}

function readCookie(name) {
  var cookies = document.cookie.split(';');
  for (var i = 0; i < cookies.length; i++) {
    var cookieParts = cookies[i].split('=');
    if (cookieParts[0].replace(/ +/g,'') === name) {
      return cookieParts[1];
    }
  }
  return null;
}

function load(id, cookieName, reverse) {
  var checkbox = document.getElementById(id);

  reverse = (reverse === undefined) ? false : reverse;
  
  if (checkbox != null) {
    cookieName = cookieName || checkbox.name;
    var status = readCookie(cookieName);
    checkbox.checked = (status === (reverse ? 'false' : 'true'));
  }
}

function check(checkbox, cookieName, reverse) {
  cookieName = cookieName || checkbox.name;
  reverse = (reverse === undefined) ? false : reverse;
  
  if (checkbox.checked) {
    createCookie(cookieName, (reverse ? 'false' : 'true'), $environment.getProperty("idp.cookie.maxAge","31536000"));
  } else {
    eraseCookie(cookieName);
  }
}

function loadRemember(rememberId, cookieName) {
  var checkbox = document.getElementById(rememberId);

  if (checkbox != null) {
    var status = readCookie(cookieName);
    checkbox.checked = (status !== '1' && status !== 'true');
  }
}

function checkRemember(checkbox, cookieName) {
  if (checkbox.checked) {
    console.log('Erasing cookie ' + cookieName);
    eraseCookie(cookieName);
  } else {
    console.log('Creating cookie ' + cookieName + '; 1; ' + $environment.getProperty("idp.cookie.maxAge","31536000"));
  }
}
